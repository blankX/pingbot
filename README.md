# Ping Bot

A Telegram bot the replies with "Pong".

## Setup
+ Install `python-telegram-bot`
    + `pip3 install python-telegram-bot`
+ Clone this repo (`git clone https://gitlab.com/blankX/pingbot.git`)
+ cd into the repo's directory (`cd pingbot`)
    + Copy `sample.config.py` to `config.py` (`cp sample.config.py config.py`)
    + Edit `config.py` with your text editor

## Start
+ `python3 bot.py &`
