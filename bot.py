#!/usr/bin/env python3
import logging,telegram
from telegram.ext import Updater, CommandHandler
try:
	import config
except ImportError:
	print('config.py is missing, have you tried `cp sample.config.py config.py`?')
	exit()

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

def start(bot, update):
    chat_id = update.message.chat_id
    custom_keyboard = [['/ping']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard)
    bot.send_message(chat_id=chat_id, text="Pong", reply_markup=reply_markup)

updater = Updater(token=config.api_key)
dispatcher = updater.dispatcher

start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

ping_handler = CommandHandler('ping', start)
dispatcher.add_handler(ping_handler)
updater.start_polling()
